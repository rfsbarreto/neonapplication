package com.example.pharol.neonapplication.contacts

data class Contact(val id: Int,val name: String, val photo: String, val phone: String)
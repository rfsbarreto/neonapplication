package com.example.pharol.neonapplication.contacts

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.pharol.neonapplication.WebService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ContactViewModel(val contactRepository: ContactRepository, val webService: WebService): ViewModel() {


    fun getContactsList() = contactRepository.getContacts()

    fun sendMoney(contact: Contact, token: String, value: Double) {
        webService.sendMoney(contact.id.toString(), contact.name, value).enqueue(object: Callback<Boolean>{
            override fun onResponse(call: Call<Boolean>?, response: Response<Boolean>?) {
                //TODO show some message to user
            }

            override fun onFailure(call: Call<Boolean>?, t: Throwable?) {
                //TODO show some message to user
            }

        })
    }

    class ContactViewModelFactory
    @Inject constructor(val contactRepository: ContactRepository, val webService: WebService): ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>?): T {
            return ContactViewModel(contactRepository, webService) as T
        }


    }




}



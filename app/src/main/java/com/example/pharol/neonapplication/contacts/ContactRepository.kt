package com.example.pharol.neonapplication.contacts

import com.example.pharol.neonapplication.R
import javax.inject.Singleton

/**
 * Created by pharol on 7/8/17.
 */
@Singleton
class ContactRepository() {

    fun getContacts(): List<Contact>{
        val listContacts = arrayListOf<Contact>()
        listContacts.add(Contact(1,"Sheev Palpatine", "http://i2.kym-cdn.com/entries/icons/facebook/000/019/930/1421657233490.jpg","(11)9999-9999"))
        listContacts.add(Contact(2,"Darth Maul", "http://vignette4.wikia.nocookie.net/starwars/images/5/50/Darth_Maul_profile.png/revision/latest?cb=20140209162228","(11)9999-9999"))
        listContacts.add(Contact(3,"Darth Tyranus", "http://vignette3.wikia.nocookie.net/starwars/images/f/f1/Count_Dooku_headshot_gaze.jpg/revision/latest?cb=20071218042012","(11)9999-9999"))
        listContacts.add(Contact(4,"Grand Admiral Thrawn", "https://images.moviepilot.com/image/upload/c_fill,h_470,q_auto:good,w_620/znbx09ldqzhpvw8k0jmf.jpg","(11)9999-9999"))
        listContacts.add(Contact(5,"Imperial Inquisitor", "https://vignette2.wikia.nocookie.net/starwars/images/1/11/TheInquisitor.png/revision/latest?cb=20141103040539","(11)9999-9999"))
        listContacts.add(Contact(6,"Agent Kallus", "https://rjreedfilms.files.wordpress.com/2016/10/star-wars-s3-ep3-2.jpg?w=616","(11)9999-9999"))
        listContacts.add(Contact(7,"Jabba the Hutt", "https://upload.wikimedia.org/wikipedia/en/5/53/Jabba_the_Hutt_in_Return_of_the_Jedi_%281983%29.png","(11)9999-9999"))
        listContacts.add(Contact(8,"Grand Moff Tarkin", "https://upload.wikimedia.org/wikipedia/en/5/5a/Grand_Moff_Tarkin.png","(11)9999-9999"))
        listContacts.add(Contact(9,"Boba Fett", "http://vignette4.wikia.nocookie.net/starwars/images/7/79/Boba_Fett_HS_Fathead.png/revision/latest?cb=20161114160631","(11)9999-9999"))
        listContacts.add(Contact(10,"Governor Pryce", "http://st.baskino.co/uploads/images/2012/046/tksi839.jpg","(11)9999-9999"))
        listContacts.add(Contact(11,"Rae Sloan", "http://vignette2.wikia.nocookie.net/starwars/images/1/15/Rae_Sloane_Orientation.png/revision/latest?cb=20150723020815","(11)9999-9999"))
        listContacts.add(Contact(12,"Kylo Ren", "http://vignette3.wikia.nocookie.net/deathbattlefanon/images/2/2a/Kylo-Ren-Profile.png/revision/latest?cb=20160602090950","(11)9999-9999"))
        listContacts.add(Contact(13,"Kylo Ren", "http://vignette3.wikia.nocookie.net/deathbattlefanon/images/2/2a/Kylo-Ren-Profile.png/revision/latest?cb=20160602090950","(11)9999-9999"))
        listContacts.add(Contact(14,"Supreme Leader Snoke", "https://vignette1.wikia.nocookie.net/starwars/images/0/08/Supreme_Leader_Snoke.png/revision/latest?cb=20160103063418","(11)9999-9999"))
        listContacts.add(Contact(15,"Captain Phasma", "https://vignette2.wikia.nocookie.net/starwars/images/e/e7/PhasmaHS-Fathead.png/revision/latest?cb=20161003052605","(11)9999-9999"))

        return listContacts
    }

}
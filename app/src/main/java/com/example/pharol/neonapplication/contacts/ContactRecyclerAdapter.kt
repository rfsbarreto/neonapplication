package com.example.pharol.neonapplication.contacts

import android.app.Dialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.pharol.neonapplication.R
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import android.graphics.drawable.Drawable
import android.graphics.Bitmap
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.Toast
import com.example.pharol.neonapplication.util.toCircleBitmap
import com.example.pharol.neonapplication.util.toMoneyString
import java.math.BigDecimal


class ContactRecyclerAdapter(val contacts: List<Contact>,val listener: SendMoney): RecyclerView.Adapter<ContactRecyclerAdapter.ViewHolder>(){

    interface SendMoney{
        fun sendMoney(contact: Contact, value: Double)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.v_contact_item,parent,false)
        return ViewHolder(view)
    }

    private var photo: Bitmap? = null



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts[position]
        holder.tv_name.text = contact.name
        holder.tv_number.text = contact.phone

        Picasso.with(holder.itemView.context.applicationContext).load(contact.photo).
                error(R.drawable.darth_vader).resize(160,160).
                into(object: Target{
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

                    override fun onBitmapFailed(errorDrawable: Drawable?) {}

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        photo = bitmap?.toCircleBitmap()
                        holder.iv_photo.setImageBitmap(photo)
                    }
                })

        holder.itemView.setOnClickListener({
            val dialog= Dialog(it.context)
            dialog.setContentView(R.layout.dialog_contact)

            val im_photo = dialog.findViewById(R.id.iv_perfil_photo) as ImageView
            im_photo.setImageBitmap(photo)

            val tv_name = dialog.findViewById(R.id.tv_name) as TextView
            tv_name.text = contact.name


            val tv_phone = dialog.findViewById(R.id.tv_phone) as TextView
            tv_phone.text = contact.phone
            val et_value = dialog.findViewById(R.id.et_value) as EditText
            et_value.addTextChangedListener(getTextWatcher(et_value))
            dialog.show()

            //TODO ClickListener do botão enviar.
            val bt_enviar = dialog.findViewById(R.id.bt_send_money)

            bt_enviar.setOnClickListener({
                val value =  et_value.text.toString().replace(Regex("[R$., ]"), "").toDouble()
                listener.sendMoney(contact, value)
                dialog.dismiss()
            })

            Toast.makeText(it.context,contact.name,Toast.LENGTH_SHORT).show()
        })
    }

    private fun getTextWatcher(et_value: EditText): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                et_value.removeTextChangedListener(this)
                var string = editable.toString().replace(Regex("[R$., ]"), "")
                val value = BigDecimal(string)
                string = value.toMoneyString()
                et_value.setText(string)
                et_value.setSelection(string.length)
                et_value.addTextChangedListener(this)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        }
    }

    override fun getItemCount() =  contacts.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv_photo by lazy { itemView.findViewById(R.id.iv_perfil_photo) as ImageView }
        val tv_name by lazy { itemView.findViewById(R.id.tv_name) as TextView }
        val tv_number by lazy { itemView.findViewById(R.id.tv_phone) as TextView }
    }
}
package com.example.pharol.neonapplication.di

import android.app.Application
import com.example.pharol.neonapplication.WebService
import com.example.pharol.neonapplication.contacts.ContactRepository
import dagger.Component


@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun getApplication(): Application

    fun getContactRepository(): ContactRepository

    fun getWebService(): WebService
}
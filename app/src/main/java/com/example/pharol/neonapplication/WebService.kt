package com.example.pharol.neonapplication

import com.example.pharol.neonapplication.transfer.Transfer
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import javax.inject.Singleton

/**
 * Created by pharol on 7/6/17.
 */
@Singleton
interface WebService{

    @GET("/GetTransfers")
    fun getTransfers(@Body token: String): Call<List<Transfer>>

    @GET("/generatetoken")
    fun getToken(@Query("nome") name: String,@Query("email") email: String): Call<String>

    @POST("/SendMoney")
    fun sendMoney(@Query("clienteID") clienteID: String, @Query("token") token: String,@Query("value") value: Double): Call<Boolean>

} 
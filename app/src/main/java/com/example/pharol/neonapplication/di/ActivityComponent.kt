    package com.example.pharol.temminstore.di.ui

import android.content.Context
import com.example.pharol.neonapplication.MainActivity
import com.example.pharol.neonapplication.contacts.ContactActivity
import com.example.pharol.neonapplication.di.ApplicationComponent
import com.example.pharol.temminstore.di.PerActivity
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class),modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(mActivity: ContactActivity)

    fun inject(mActivity: MainActivity)

    fun getApplication() : Context

}
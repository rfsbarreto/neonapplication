package com.example.pharol.neonapplication.contacts

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import com.example.pharol.neonapplication.NeonApplication
import com.example.pharol.neonapplication.R
import com.example.pharol.temminstore.di.ui.ActivityModule
import com.example.pharol.temminstore.di.ui.DaggerActivityComponent
import javax.inject.Inject

class ContactActivity : AppCompatActivity(), ContactRecyclerAdapter.SendMoney {


    val component by lazy { DaggerActivityComponent.builder().activityModule(ActivityModule(this)).
            applicationComponent((this.applicationContext as NeonApplication).component).build() }

    lateinit var contactViewModel: ContactViewModel

    @Inject lateinit var contactViewModelFactory: ContactViewModel.ContactViewModelFactory

    val rvContacts by lazy { findViewById(R.id.rv_contacts) as RecyclerView }
    val progressBar by lazy {findViewById(R.id.progressBar) as ProgressBar }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)
        component.inject(this)
        val toolbar= findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        contactViewModel = ViewModelProviders.of(this,contactViewModelFactory).get(ContactViewModel::class.java)

        rvContacts.layoutManager = LinearLayoutManager(this)
        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        divider.setDrawable(getDrawable(R.drawable.divisor))
        rvContacts.addItemDecoration(divider)
        rvContacts.adapter = ContactRecyclerAdapter(contactViewModel.getContactsList(),this)
        progressBar.visibility = View.GONE

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun sendMoney(contact: Contact, value: Double) {
        val sharedPref = getSharedPreferences(getString(R.string.preference_file_token), Context.MODE_PRIVATE)

        contactViewModel.sendMoney(contact, sharedPref.getString(getString(R.string.token),"") , value)
    }
}

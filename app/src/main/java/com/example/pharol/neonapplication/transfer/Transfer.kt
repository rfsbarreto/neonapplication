package com.example.pharol.neonapplication.transfer

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.math.BigDecimal
import java.util.*

@Entity
data class Transfer(@PrimaryKey(autoGenerate = true) val id: Int,
                    val clientID: String,
                    val token: String,
                    val value: BigDecimal,
                    val date: Date)
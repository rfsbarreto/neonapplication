package com.example.pharol.neonapplication.di

import android.app.Application
import com.example.pharol.neonapplication.WebService
import com.example.pharol.neonapplication.contacts.ContactRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory


/**
 * Created by pharol on 7/8/17.
 */
@Module
class ApplicationModule(val application: Application){

    @Provides
    fun provideApplication(): Application = application

    @Provides
    fun provideContactRepository(): ContactRepository = ContactRepository()

    @Provides
    fun provideWebService(): WebService = Retrofit.Builder().baseUrl("http://processoseletivoneon.azurewebsites.net/").
            addConverterFactory(GsonConverterFactory.create()).build().create(WebService::class.java)
}
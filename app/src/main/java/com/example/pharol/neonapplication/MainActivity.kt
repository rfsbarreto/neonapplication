package com.example.pharol.neonapplication

import android.content.Context 
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.example.pharol.neonapplication.contacts.ContactActivity
import com.example.pharol.neonapplication.di.ApplicationModule
import com.example.pharol.neonapplication.di.DaggerApplicationComponent
import com.example.pharol.neonapplication.util.toCircleBitmap
import com.example.pharol.temminstore.di.ui.ActivityModule
import com.example.pharol.temminstore.di.ui.DaggerActivityComponent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    val component  by lazy{  DaggerActivityComponent.builder().activityModule(ActivityModule(this)).
            applicationComponent((this.applicationContext as NeonApplication).component).build() }

    val img_photo by lazy { findViewById(R.id.iv_perfil_photo) as ImageView }
    val contactButton by lazy { findViewById(R.id.bt_send_money) as Button }
    @Inject lateinit var webService: WebService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)


        img_photo.setImageBitmap(BitmapFactory.decodeResource(resources,R.drawable.darth_vader).toCircleBitmap())

        contactButton.setOnClickListener(View.OnClickListener {
            webService.getToken(resources.getString(R.string.name),resources.getString(R.string.email)).enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>?, response: Response<String>) {
                    saveToken(response.body()!!)
                    startActivity(Intent(baseContext,ContactActivity::class.java))
                }

                override fun onFailure(call: Call<String>?, t: Throwable?) {
                    Toast.makeText(baseContext,"Não foi possível se conecar ao servidor. Tente mais tarde!",Toast.LENGTH_SHORT).show()
                    Log.v("teste",t?.message)
                }

            })
        })

        //TODO ClickListener do botão Transfer History.

    }

    private fun saveToken(token: String) {
        val sharedPref = baseContext.getSharedPreferences(getString(R.string.preference_file_token), Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(getString(R.string.token), token)
        editor.commit()
    }
}

package com.example.pharol.neonapplication

import android.app.Application
import com.example.pharol.neonapplication.di.ApplicationModule
import com.example.pharol.neonapplication.di.DaggerApplicationComponent

class NeonApplication: Application(){

    val component  = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()

    override fun onCreate() {
        super.onCreate()
    }
} 
package com.example.pharol.neonapplication.util

import android.graphics.*
import java.math.BigDecimal
import java.util.*


fun  Bitmap.toCircleBitmap(): Bitmap {
    val bitMap = Bitmap.createBitmap(this.width,this.height, Bitmap.Config.ARGB_8888)
    val borderWidth = 4
    val canvas = Canvas(bitMap)

    val color = 0xffffffff.toInt()
    val paint = Paint()
    val rect = Rect(0, 0, this.width, this.height)

    paint.isAntiAlias = true


    canvas.drawCircle(this.width / 2F, this.height / 2F,
            this.width / 2F - borderWidth, paint)

    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)

    canvas.drawBitmap(this, rect, rect, paint)

    return bitMap
}

fun BigDecimal.toMoneyString(): String {
    val sb = StringBuilder()
    Formatter(sb, Locale.GERMANY).format("R$ %,.2f",this.divide(BigDecimal("100"))) //Converting the price Bigdecimal to a money Format, decided to divide per 100 because after all a shirt doesn't cost 7990
    return sb.toString()
} 